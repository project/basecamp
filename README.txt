-- SUMMARY --

This module aims to implement the API provided by Basecamp, an online project
management system.

-- IMPLEMENTATION PLAN --

Initially, create a block that shows the current user's tasks and the ability to
create tasks that get submitted to Basecamp.

-- REQUIREMENTS --

- An account with http://basecamphq.com/
- PHP installed with SimpleXML support. It is enabled by default in PHP since
version 5.1.2

-- INSTALLATION --

After enabling the module, visit admin/config/services/basecamp to configure
settings. If you would like to allow users to submit tasks to Basecamp from
Drupal, visit admin/config/services/basecamp/projects.
