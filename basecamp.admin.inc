<?php

/**
 * @file
 * Functions relating to the admin of this module
 */

/**
 * Form constructor for the admin settings form.
 *
 * @see basecamp_menu()
 */
function basecamp_admin_form($form, &$form_state) {
  $form = array();

  $form['basecamp_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Basecamp URL'),
    '#description' => t('The web address that you access Basecamp over e.g. mycompany.basecamphq.com'),
    '#default_value' => variable_get('basecamp_url'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['basecamp_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Use HTTP or HTTPS?'),
    '#description' => t("If SSL is enabled for your account, ensure that you're using https. If it's not, ensure you're using http."),
    '#options' => drupal_map_assoc(array('http://', 'https://')),
    '#default_value' => variable_get('basecamp_protocol', 'https://'),
  );

  $form['basecamp_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API Token'),
    '#description' => t('The API token can be found by logging in to your Basecamp account, clicking My Info (top right) then clicking Show tokens (near the bottom)'),
    '#default_value' => variable_get('basecamp_token'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Form constructor for configuring projects & task lists available.
 *
 * @see basecamp_menu()
 */
function basecamp_admin_projects_form($form, &$form_state) {
  $form = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="project-admin">',
    '#suffix' => '</div>',
  );

  // Display current config items.
  $query = db_select('basecamp_todo_config', 'btc')
    ->fields('btc')
    ->orderBy('config_name', 'ASC');
  $result = $query->execute();
  foreach ($result as $record) {
    $form['current'][$record->config_id]['config_name'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->config_name,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['project_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->project_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['todo_list_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->todo_list_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['role_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->role_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['person_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->person_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['remove'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'remove' . $record->config_id,
      '#submit' => array('basecamp_admin_projects_form_del_submit'),
      '#limit_validation_errors' => array(
        array('current'),
      ),
    );
  }

  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new configuration'),
  );

  $form['add']['config_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Give this configuration a name'),
    '#required' => TRUE,
  );

  // Get list of projects.
  $project_list = basecamp_submit_request('/projects.xml');
  $projects = array();
  foreach ($project_list->{'project'} as $project) {
    $projects[(int) $project->id] = (string) $project->name;
  }
  asort($projects);
  $projects = array(0 => '---') + $projects;
  $form['add']['project_list'] = array(
    '#type' => 'select',
    '#title' => t('Project'),
    '#options' => $projects,
    '#ajax' => array(
      'callback' => 'basecamp_admin_projects_form_callback',
      'wrapper' => 'project-admin',
    ),
  );

  // Get a list of todo lists if a project has been chosen.
  if (isset($form_state['values']['add']['project_list'])) {
    $todo_lists = basecamp_submit_request('/projects/' . $form_state['values']['add']['project_list'] . '/todo_lists.xml');
    $tasks = array();
    foreach ($todo_lists as $todo_list) {
      $tasks[(int) $todo_list->id] = (string) $todo_list->name;
    }
  }
  else {
    $tasks = array(t('Choose a project first'));
  }
  $form['add']['todo_list'] = array(
    '#type' => 'select',
    '#title' => t('To-Do List'),
    '#options' => $tasks,
  );

  // Get a list of roles.
  $roles = user_roles(TRUE);
  $role_list = array();
  foreach ($roles as $id => $role) {
    $role_list[$id] = $role;
  }
  $form['add']['permission'] = array(
    '#type' => 'select',
    '#title' => t('Who has permission to submit tasks to this list?'),
    '#options' => $role_list,
  );

  // Get a list of users.
  if (isset($form_state['values']['add']['project_list'])) {
    $people = basecamp_submit_request('/projects/' . $form_state['values']['add']['project_list'] . '/people.xml');
    $person_list = array();
    foreach ($people as $person) {
      $person_list[(int) $person->id] = (string) $person->{'first-name'} . ' ' . (string) $person->{'last-name'};
    }
    asort($person_list);
    $person_list = array(0 => t('Anyone')) + $person_list;
  }
  else {
    $person_list = array(t('Choose a project first'));
  }
  $form['add']['asignee'] = array(
    '#type' => 'select',
    '#title' => t('Who should these tasks be assigned to?'),
    '#options' => $person_list,
  );

  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('basecamp_admin_projects_form_add_submit'),
  );

  return $form;
}

/**
 * Form submission handler for basecamp_admin_projects_form().
 */
function basecamp_admin_projects_form_add_submit($form, $form_state) {
  $config_id = db_insert('basecamp_todo_config')
    ->fields(array(
      'config_name' => check_plain($form_state['values']['add']['config_name']),
      'project_id' => $form_state['values']['add']['project_list'],
      'todo_list_id' => $form_state['values']['add']['todo_list'],
      'role_id' => $form_state['values']['add']['permission'],
      'person_id' => $form_state['values']['add']['asignee'],
    ))
    ->execute();
  drupal_set_message(t('Configuration %config_name saved.', array('%config_name' => $form_state['values']['add']['config_name'])));
}

/**
 * Returns form definition for use in AJAX callback.
 *
 * @see basecamp_admin_projects_form()
 */
function basecamp_admin_projects_form_callback($form, $form_state) {
  return $form;
}

/**
 * Form submission handler for Remove buttons in basecamp_admin_projects_form().
 */
function basecamp_admin_projects_form_del_submit($form, $form_state) {
  $config_id = $form_state['triggering_element']['#parents'][1];
  $query = db_delete('basecamp_todo_config')
    ->condition('config_id', $config_id)
    ->execute();
  drupal_set_message(t('Configuration %config_name removed.', array('%config_name' => $form_state['values']['current'][$config_id]['config_name'])));
}

/**
 * Form constructor for configuring links between Drupal and Basecamp users.
 *
 * @see basecamp_menu()
 */
function basecamp_admin_people_form($form, &$form_state) {
  $form = array(
    '#tree' => TRUE,
  );

  // Display current pairings.
  $query = db_select('basecamp_people', 'bp')
    ->fields('bp');
  $result = $query->execute();
  foreach ($result as $record) {
    $form['current'][$record->config_id]['basecamp_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->basecamp_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['drupal_id'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $record->drupal_id,
      '#disabled' => TRUE,
    );
    $form['current'][$record->config_id]['remove'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'remove' . $record->config_id,
      '#submit' => array('basecamp_admin_people_form_del_submit'),
      '#limit_validation_errors' => array(
        array('current'),
      ),
    );
  }

  // Build an array of Basecamp users.
  $people = basecamp_submit_request('/people.xml');
  $basecamp_list = array();
  foreach ($people->person as $person) {
    $basecamp_list[(int) $person->id] = $person->{'first-name'} . ' ' . $person->{'last-name'};
  }
  asort($basecamp_list);

  // Build an array of Drupal users.
  $drupal_users = user_load_multiple(FALSE);
  unset($drupal_users[0]);
  $drupal_list = array();
  foreach ($drupal_users as $user) {
    $drupal_list[$user->uid] = $user->name;
  }
  asort($drupal_list);

  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new pairing'),
  );

  $form['add']['basecamp_user'] = array(
    '#type' => 'select',
    '#title' => t('Pick a Basecamp user'),
    '#options' => $basecamp_list,
  );

  $form['add']['drupal_user'] = array(
    '#type' => 'select',
    '#title' => t('Pick a Drupal user'),
    '#options' => $drupal_list,
  );

  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('basecamp_admin_people_form_add_submit'),
  );

  return $form;
}

/**
 * Form submission handler for Add button in basecamp_admin_people_form().
 */
function basecamp_admin_people_form_add_submit($form, $form_state) {
  $config_id = db_insert('basecamp_people')
    ->fields(array(
      'basecamp_id' => $form_state['values']['add']['basecamp_user'],
      'drupal_id' => $form_state['values']['add']['drupal_user'],
    ))
    ->execute();
  drupal_set_message(t('Pairing %basecamp_name / %drupal_name saved.', array(
    '%basecamp_name' => $form['add']['basecamp_user']['#options'][$form_state['values']['add']['basecamp_user']],
    '%drupal_name' => $form['add']['drupal_user']['#options'][$form_state['values']['add']['drupal_user']],
  )));
}

/**
 * Form submission handler for Remove buttons in basecamp_admin_people_form().
 */
function basecamp_admin_people_form_del_submit($form, $form_state) {
  $config_id = $form_state['triggering_element']['#parents'][1];
  $query = db_delete('basecamp_people')
    ->condition('config_id', $config_id)
    ->execute();
  drupal_set_message(t('Pairing removed.'));
}

/**
 * Displays a summary of Basecamp account info.
 *
 * Path: admin/reports/basecamp
 *
 * @see basecamp_menu()
 */
function basecamp_account_report() {
  $account_info = basecamp_submit_request('/account.xml');
  $output = '<h2>' . t('Account Info') . '</h2>';
  $output .= '<p>' . t('This is an overview of your Basecamp account settings.') . '</p>';
  $output .= basecamp_simplexml_to_html($account_info);
  return $output;
}

/**
 * Displays a list of Basecamp people and the matching Drupal user.
 *
 * Path: admin/reports/basecamp/people
 *
 * @see basecamp_menu()
 */
function basecamp_people_report() {
  // Get saved pairings.
  $pairs = db_select('basecamp_people', 'bp')
    ->fields('bp')
    ->execute()
    ->fetchAllAssoc('basecamp_id');

  $header = array(t('Basecamp account'), t('Drupal account'));
  $rows = array();
  $people = basecamp_submit_request('/people.xml');
  foreach ($people->person as $person) {
    $row_data = array($person->{'first-name'} . ' ' . $person->{'last-name'});
    if (!empty($pairs[(int) $person->id])) {
      $user = user_load($pairs[(int) $person->id]->drupal_id);
      $row_data[] = t('%user_name (Manual)', array('%user_name' => $user->name));
    }
    elseif ($user = user_load_by_mail($person->{'email-address'})) {
      $row_data[] = $user->name;
    }
    else {
      $row_data[] = '';
    }
    $rows[] = array('data' => $row_data);
  }

  $output = '<h2>' . t('Account Info') . '</h2>';
  $output .= '<p>' . t('This is an overview of how your Basecamp users are paired with your Drupal users based on email address.') . '</p>';
  $output .= '<p>' . t('You can override pairings in the <a href="@link">module config</a>', array('@link' => '/admin/config/services/basecamp/people')) . '</p>';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}
