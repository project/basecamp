<?php

/**
 * @file
 * Functions controlling the theme output of this module.
 */

/**
 * Returns HTML for the project config page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The form definition that is being formatted.
 *
 * @see basecamp_admin_projects_form()
 */
function theme_basecamp_admin_projects_form($variables) {
  $form = $variables['form'];
  $output = '';

  if (!empty($form['current'])) {
    $header = array(
      'Config Name',
      'Project',
      'To-Do List',
      'Permission',
      'Asignee',
      'Remove',
    );
    $rows = array();
    foreach ($form['current'] as $config_id => $config) {
      if (!is_int($config_id)) {
        continue;
      }
      $row_data = array(
        drupal_render($form['current'][$config_id]['config_name']),
        drupal_render($form['current'][$config_id]['project_id']),
        drupal_render($form['current'][$config_id]['todo_list_id']),
        drupal_render($form['current'][$config_id]['role_id']),
        drupal_render($form['current'][$config_id]['person_id']),
        drupal_render($form['current'][$config_id]['remove']),
      );
      $rows[] = array('data' => $row_data);
    }
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }

  // Render anything left and return the HTML.
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Returns HTML for the people config page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The form definition that is being formatted.
 *
 * @see basecamp_admin_people_form()
 */
function theme_basecamp_admin_people_form($variables) {
  $form = $variables['form'];
  $output = '';

  if (!empty($form['current'])) {
    $header = array('Basecamp User', 'Drupal User', 'Remove');
    $rows = array();
    foreach ($form['current'] as $config_id => $config) {
      if (!is_int($config_id)) {
        continue;
      }
      $row_data = array(
        drupal_render($form['current'][$config_id]['basecamp_id']),
        drupal_render($form['current'][$config_id]['drupal_id']),
        drupal_render($form['current'][$config_id]['remove']),
      );
      $rows[] = array('data' => $row_data);
    }
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }

  // Render anything left and return the HTML.
  $output .= drupal_render_children($form);
  return $output;
}
