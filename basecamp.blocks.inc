<?php

/**
 * @file
 * Functions relating to this modules use of blocks (except hooks).
 */

/**
 * Defines the content for the basecamp_tasks block.
 *
 * @see basecamp_block_view()
 */
function basecamp_block_basecamp_tasks() {
  $output = '';

  $basecamp_user = basecamp_get_id();

  if ($basecamp_user && $request = basecamp_submit_request('/todo_lists.xml?responsible_party=' . $basecamp_user)) {
    $url = variable_get('basecamp_protocol', 'https://') . variable_get('basecamp_url');

    foreach ($request->{'todo-list'} as $todo_list) {
      $output .= '<strong>' . $todo_list->name . '</strong>';
      $output .= '<ul>';
      foreach ($todo_list->{'todo-items'}->children() as $todo_item) {
        $link = $url . '/projects/' . $todo_list->{'project-id'} . '/todo_items/' . $todo_item->id . '/comments';
        $output .= t('<li>@item (<a href="@link">edit</a>)</li>', array('@item' => $todo_item->content, '@link' => $link));
      }
      $output .= '</ul>';
    }
  }

  return $output;
}

/**
 * Defines the content for the basecamp_create_task block.
 *
 * This function is called by drupal_get_form().
 *
 * @see basecamp_block_view()
 */
function basecamp_block_basecamp_create_task($form, &$form_state) {
  global $user;

  $form = array(
    '#tree' => TRUE,
  );

  // Get available todo config.
  $query = db_select('basecamp_todo_config', 'btc')
    ->fields('btc')
    ->orderBy('config_name', 'ASC');
  $result = $query->execute();
  $options = array();
  foreach ($result as $record) {
    if (in_array($record->role_id, array_keys($user->roles))) {
      $options[$record->config_id] = check_plain($record->config_name);
    }
  }
  $form['list'] = array(
    '#type' => 'select',
    '#title' => t('Which to-do list is your task for?'),
    '#options' => $options,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Please describe your request'),
    '#cols' => 25,
    '#rows' => 6,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Form submission handler for _basecamp_block_basecamp_create_task().
 */
function basecamp_block_basecamp_create_task_submit($form, $form_state) {
  global $user;

  // Get to-do list config.
  $result = db_select('basecamp_todo_config', 'btc')
    ->fields('btc')
    ->condition('config_id', $form_state['values']['list'])
    ->execute()
    ->fetchAssoc();

  // Get the task template.
  $url = '/todo_lists/' . $result['todo_list_id'] . '/todo_items/new.xml';
  $new_task = basecamp_submit_request($url);

  // Add task details.
  $new_task->content = 'From ' . $user->name . ': ' . $form_state['values']['description'];
  if ($result['person_id']) {
    $new_task->responsible_party = $result['person_id'];
    $new_task->notify = TRUE;
  }

  // Submit to Basecamp.
  $submitted_task = basecamp_submit_request('/todo_lists/' . $result['todo_list_id'] . '/todo_items.xml', 'POST', $new_task);
  drupal_set_message(t('Task submitted.'));
}
