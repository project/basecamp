<?php

/**
 * @file
 * Basecamp primary module file.
 */

/**
 * Implements hook_menu().
 */
function basecamp_menu() {
  $items = array();

  $items['admin/config/services/basecamp'] = array(
    'title' => 'Basecamp',
    'description' => 'Configure settings for the Basecamp API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('basecamp_admin_form'),
    'access arguments' => array('administer basecamp module'),
    'file' => 'basecamp.admin.inc',
  );

  $items['admin/config/services/basecamp/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access arguments' => array('administer basecamp module'),
    'weight' => 10,
  );

  $items['admin/config/services/basecamp/projects'] = array(
    'title' => 'Projects',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('basecamp_admin_projects_form'),
    'access arguments' => array('administer basecamp module'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'basecamp.admin.inc',
    'weight' => 20,
  );

  $items['admin/config/services/basecamp/people'] = array(
    'title' => 'People',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('basecamp_admin_people_form'),
    'access arguments' => array('administer basecamp module'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'basecamp.admin.inc',
    'weight' => 30,
  );

  $items['admin/reports/basecamp'] = array(
    'title' => 'Basecamp',
    'description' => 'View reports about your Basecamp account.',
    'page callback' => 'basecamp_account_report',
    'access arguments' => array('administer basecamp module'),
    'file' => 'basecamp.admin.inc',
  );

  $items['admin/reports/basecamp/account'] = array(
    'title' => 'Account Info',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 10,
  );

  $items['admin/reports/basecamp/people'] = array(
    'title' => 'People',
    'page callback' => 'basecamp_people_report',
    'access arguments' => array('administer basecamp module'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'basecamp.admin.inc',
    'weight' => 20,
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function basecamp_block_info() {
  $blocks = array();

  $blocks['basecamp_tasks'] = array(
    'info' => t('Basecamp Tasks'),
  );

  $blocks['basecamp_create_task'] = array(
    'info' => t('Submit Basecamp Task'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function basecamp_block_view($delta = '') {
  module_load_include('inc', 'basecamp', 'basecamp.blocks');
  $block = array();

  switch ($delta) {
    case 'basecamp_tasks':
      $block['subject'] = t('Your Tasks');
      $block['content'] = basecamp_block_basecamp_tasks();
      break;

    case 'basecamp_create_task':
      $block['subject'] = t('Submit a Task');
      $block['content'] = drupal_get_form('basecamp_block_basecamp_create_task');
      break;
  }

  return $block;
}

/**
 * Implements hook_ctools_block_info().
 */
function basecamp_ctools_block_info($module, $delta, &$info) {
  $info['category'] = t('Basecamp');
  return $info;
}

/**
 * Implements hook_theme().
 */
function basecamp_theme($existing, $type, $theme, $path) {
  return array(
    'basecamp_admin_projects_form' => array(
      'render element' => 'form',
      'file' => 'basecamp.theme.inc',
    ),
    'basecamp_admin_people_form' => array(
      'render element' => 'form',
      'file' => 'basecamp.theme.inc',
    ),
  );
}

/**
 * Implements hook_permission().
 */
function basecamp_permission() {
  return array(
    'administer basecamp module' => array(
      'title' => t('Administer Basecamp module'),
      'description' => t('Perform administration tasks for Basecamp module.'),
    ),
  );
}

/**
 * Submits a request to Basecamp.
 *
 * @param string $path
 *   The URL path to use for the request, excluding domain. Leading / is
 *   optional e.g. /projects.xml or projects.xml
 * @param string $method
 *   HTTP method to use.
 * @param object|NULL $data
 *   Data to submit with request, or NULL if not needed.
 *
 * @return object|bool
 *   SimpleXML object if given, TRUE on success, or FALSE on error or failure.
 */
function basecamp_submit_request($path, $method = 'GET', $data = NULL) {
  // Check if the module has been configured.
  if (variable_get('basecamp_token', '') == '') {
    drupal_set_message(t("Sorry, the Basecamp module hasn't been configured with an API token yet."), 'error');
    return FALSE;
  }

  // Use the token as the username, and any password.
  $url = variable_get('basecamp_protocol', 'https://') . variable_get('basecamp_token') . ':X@' . variable_get('basecamp_url');

  // Append the path.
  if (substr($path, 0, 1) != '/') {
    $path = '/' . $path;
  }
  $url .= $path;

  // Basic request headers.
  $options = array(
    'headers' => array(
      'Content-Type' => 'application/xml',
      'Accept' => 'application/xml',
    ),
  );

  // Add extra request options for specified method.
  switch ($method) {
    case 'GET':
      break;

    case 'POST':
      $options += array(
        'method' => 'POST',
        'data' => $data->asXML(),
      );
      break;
  }

  // Submit to Basecamp.
  $reply = drupal_http_request($url, $options);

  // Check response.
  if ($reply->code == 200) {
    return simplexml_load_string($reply->data);
  }
  elseif ($reply->code == 201) {
    return TRUE;
  }
  else {
    drupal_set_message(t('@error_message (@error_code)', array(
      '@error_message' => $reply->error,
      '@error_code' => $reply->code)
    ), 'error');
    return FALSE;
  }
}

/**
 * Returns the Basecamp ID for the currently logged-in user.
 *
 * @return int|bool
 *   A Basecamp ID, or FALSE if no matching user is found.
 */
function basecamp_get_id() {
  global $user;

  // Check if we have a stored user pairing.
  $result = db_select('basecamp_people', 'bp')
    ->fields('bp')
    ->condition('drupal_id', $user->uid)
    ->execute()
    ->fetchAssoc();
  if ($result) {
    return $result['basecamp_id'];
  }

  // If no pairing has been saved, search based on email.
  $people = basecamp_submit_request('/people.xml');
  foreach ($people->person as $person) {
    if ($person->{'email-address'} == $user->mail) {
      return (int) $person->id;
    }
  }

  return FALSE;
}

/**
 * Returns an HTML unordered list of properties from a SimpleXML object.
 */
function basecamp_simplexml_to_html($account_info) {
  $return = '<ul>';
  foreach ($account_info->children() as $item => $info) {
    if ($info->count() > 0) {
      $return .= '<li>' . $item . '';
      $return .= basecamp_simplexml_to_html($info);
      $return .= '</li>';
    }
    else {
      $return .= '<li>' . $item . ' = ' . $info . '</li>';
    }
  }
  $return .= '</ul>';
  return $return;
}
